<?php
/**
 * Facebook Bundle for Symfony2
 *
 * @author Stefan Matei (stefan@intelligentbee.com)
 * @since 2014
 */

namespace Ibw\FacebookBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * This is the class that validates and merges configuration from your app/config files
 *
 * @package Ibw\FacebookBundle\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('facebook_api');
        $rootNode
            ->children()
                ->scalarNode('api_id')
                    ->cannotBeEmpty()
                    ->isRequired()
                ->end()
                ->arrayNode('api_configuration')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('https')
                            ->defaultValue(false)
                        ->end()
                        ->scalarNode('base_currency')
                            ->defaultValue('USD')
                        ->end()
                    ->end()
            ->end();

        return $treeBuilder;
    }
}

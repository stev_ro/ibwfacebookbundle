<?php
/**
 * Facebook Bundle for Symfony2
 *
 * @author Stefan Matei (stefan@intelligentbee.com)
 * @since 2014
 */

namespace Ibw\FacebookBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FacebookBundle extends Bundle
{

}